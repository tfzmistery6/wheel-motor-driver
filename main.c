/******************************************************************************
 * @file    main.c
 * @brief   Главный файл проекта системы управления трёхфазным драйвером
 * @version v0.1
 * @date    27.01.2022
 * @author  Власовский Алексей Игоревич & Мануйлов Александр Владимирович
 * @note    АО "ОКБ МЭЛ" г.Калуга
 ******************************************************************************/

#include "stm32g474xx.h"
#include "SysTick_.h"
#include "var_.h"
#include "gpio_.h"
#include "usart_.h"
#include "tim_.h"
#include "adc_.h"
#include "exti_.h"
#include "rcc_.h"
#include "dma_.h"

/*____________________PROTOTYPES____________________*/
void global_var_init(void); //дополнительная инициализация переменных
int kalman_filter(uint16_t y, uint16_t m, float k); //Цифровая фильтрация
/*___Функциональные блоки___*/


/*___Коммуникации___*/
void clear_RXBuffer(void); //Очистка буфера принятых символов
char * utoa_div(uint32_t value, char *buffer); //преобразование uint to ascii через %
int is_equal(char *string1, char *string2, int len); //Функция сравнения двух строк (замена strncmp)
int is_digit(char byte); //функция проверки является ли принятый байт цифрой в ascii кодировке
int get_param(char *buf); //выделение параметров из буфера
void USART1_TXBuf_append (char *buffer); //Отправка данных в USART1
void RXBuffer_Handler (void); //Обработчик буфера принятых символов после приёма CR

int main() {
	//"включение FPU"
	SCB -> CPACR |= (3 << 20 | 3 << 22); //CP10 CP11 Full access
	sysclk_cfgr(); //настройка тактирования контроллера
	
	global_var_init();
	gpio_init();
	usart1_init();
	SysTick_init();
	exti_init();
	tim1_init();//ШИМ фаз
	dma1_init();//Инициализация DMA1
	adc1_init();//Измерение тока
	adc2_init();//Измерение ручки скважности
	adc4_init();//Измерение температуры
	
	tim7_init();//Обработчик машины состояний системы управления
	tim5_init();//Вспомогательный обработчик
	tim15_init();//Таймер для измерения скорости вращения (побыстрее)
	tim16_init();//Таймер для определения состояния ротора (помедленней)
	
	//Запускаем прерывания
	NVIC_EnableIRQ(USART1_IRQn);
	NVIC_EnableIRQ(TIM5_IRQn);
	NVIC_SetPriority(TIM5_IRQn, 1); //приоритет понижен, что позволит оперативно пересылать данные
	NVIC_EnableIRQ(TIM7_DAC_IRQn);
	NVIC_EnableIRQ(EXTI9_5_IRQn); //Прерывание для резервного датчика Холла
	NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
	
	
	while(1);
}

/*_______________INTERRUPTS HANDLERS_______________*/

//Прерывание по переполнению таймера TIM7 (16 кГц)
void TIM7_DAC_IRQHandler (void) {
	TIM7 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
	
	//GPIOD -> ODR |= GPIO_ODR_OD2;
	GPIOB -> ODR |= GPIO_ODR_OD6;
	
	HS1_state_prev = HS1_state;
	HS2_state_prev = HS2_state;
	HS3_state_prev = HS3_state;
	HS1_state = ((GPIOA -> IDR & GPIO_IDR_ID10) >> 10);
	HS2_state = ((GPIOA -> IDR & GPIO_IDR_ID9) >> 9);
	HS3_state = ((GPIOA -> IDR & GPIO_IDR_ID8) >> 8);
	
	brake_1 = ((GPIOB -> IDR & GPIO_IDR_ID6) >> 6);
	brake_2 = 1 - ((GPIOB -> IDR & GPIO_IDR_ID7) >> 7);
	
		
	throttle = (raw_throttle >> 4) + (throttle - (throttle >> 4)); //бинарная фильтрация
	
//	//блок обработки сигнала с ручки и растягивание его диапазона до [0, 255]
//	if((throttle < 2960) && (throttle >= 2150)) {
//		cmpa_val = (int)((2960-throttle)/12) + minimum_cmpa;
//	} else if((throttle < 2150) && (throttle > 1100)) {
//		cmpa_val = (int)((2150-throttle)/6) + 68 + minimum_cmpa;
//	}
	
	//блок обработки сигнала с ручки и растягивание его диапазона до [0, 140]
	if((throttle < 2960) && (throttle >= 2150)) {
		cmpa_val = (int)((2960-throttle)/23) + minimum_cmpa;
	} else if((throttle < 2150) && (throttle > 1100)) {
		cmpa_val = (int)((2150-throttle)/6) + 35 + minimum_cmpa;
	}
	else {
		cmpa_val = 0;
	}
	if(cmpa_val > 255){cmpa_val = 255;}
	
	if(brake_2 == 1) {
		cmpa_val = 0;
	}
	 
	//блок контроля спада ШИМ
	if(cmpa_val <= old_cmpa_val){
		cmpa_val = old_cmpa_val;
		delta_counter++;
		
		if(delta_counter >= (230 - 50 * brake_2)) {
			delta_counter = 0;
			old_cmpa_val--;
			if (old_cmpa_val <= 1) {
				old_cmpa_val = 1;
			}
			cmpa_val = old_cmpa_val;
		}
		
		//Рассчёт кривой торможения
		brake_cmpa = (int) ((104300/rotor_speed) - (5 + 15 * brake_2));
		
		if (brake_cmpa < minimum_cmpa){
			brake_cmpa = minimum_cmpa;
		}
		
		if (brake_cmpa > 210){
			brake_cmpa = 210;
		}
		
		//если скорость вращения колеса больше 50 об/мин
		if(rotor_speed < 6000){
			//То продолжаем удерживать ШИМ выше кривой
			if(cmpa_val < brake_cmpa){
				cmpa_val = brake_cmpa;
			}
		}
	}
	else {
		delta_counter = 0;
	}
	
	//блок контроля нарастания ШИМ
	if(cmpa_val > old_cmpa_val){
		cmpa_val = old_cmpa_val;
		delta2_counter++;
		
		if(delta2_counter >= 230) {
			delta2_counter = 0;
			old_cmpa_val++;
			cmpa_val = old_cmpa_val;
		}
	}
	else {
		delta2_counter = 0;
	}
	
	//Защита от повёрнутой ручки газа при включениии
	if ((cmpa_val < minimum_cmpa) && (throttle_state != OK)) {
		throttle_state_counter++;
		
		if (throttle_state_counter > 30000) {
			throttle_state = OK;
		}
	}
	
	//Флаг по превышению тока
	if ((phase_current[0] > max_current) || (phase_current[0] < min_current)) {
		overcur_flag = 1;
	}
	
	if ((phase_current[1] > max_current) || (phase_current[1] < min_current)) {
		overcur_flag = 1;
	}
		
	if ((phase_current[2] > max_current) || (phase_current[2] < min_current)) {
		overcur_flag = 1;
	}
	
	//Если ручка слегка повёрнута и с ручкой порядок
	if ((cmpa_val > minimum_cmpa) && (throttle_state == OK)) {
		
		//Если заметили большой ток, сбавим ШИМ
		if(overcur_flag) {
			if (cmpa_val >= minimum_cmpa) {
				cmpa_val--;
			}
			overcur_flag = 0;
		}
		
		if((HS1_state == 1) && (HS2_state == 0) && (HS3_state == 0)) {
			TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1 шим
			TIM1 -> CCER &= ~(TIM_CCER_CC2E | TIM_CCER_CC2NE); //канал 2 выкл
			TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3 земля
			TIM1 -> CCR1 = ARELOAD_VAL - cmpa_val;
			TIM1 -> CCR2 = 0;
			TIM1 -> CCR3 = ARELOAD_VAL;
		}
		
		if((HS1_state == 1) && (HS2_state == 1) && (HS3_state == 0))
		{
			TIM1 -> CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC1NE); //канал 1 выкл
			TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2 вкл шим
			TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3 вкл земля
			TIM1 -> CCR1 = 0;
			TIM1 -> CCR2 = ARELOAD_VAL - cmpa_val;
			TIM1 -> CCR3 = ARELOAD_VAL;
		}
		
		if((HS1_state == 0) && (HS2_state == 1) && (HS3_state == 0))
		{
			TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1 земля
			TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2 вкл шим
			TIM1 -> CCER &= ~(TIM_CCER_CC3E | TIM_CCER_CC3NE); //канал 3 выкл
			TIM1 -> CCR1 = ARELOAD_VAL;
			TIM1 -> CCR2 = ARELOAD_VAL - cmpa_val;
			TIM1 -> CCR3 = 0;
		}
		
		if((HS1_state == 0) && (HS2_state == 1) && (HS3_state == 1))
		{
			TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1 земля
			TIM1 -> CCER &= ~(TIM_CCER_CC2E | TIM_CCER_CC2NE); //канал 2 выкл
			TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3 шим
			TIM1 -> CCR1 = ARELOAD_VAL;
			TIM1 -> CCR2 = 0;
			TIM1 -> CCR3 = ARELOAD_VAL - cmpa_val;
		}
		
		if((HS1_state == 0) && (HS2_state == 0) && (HS3_state == 1))
		{
			TIM1 -> CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC1NE); //канал 1 выкл
			TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2 земля
			TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3 шим
			TIM1 -> CCR1 = 0;
			TIM1 -> CCR2 = ARELOAD_VAL;
			TIM1 -> CCR3 = ARELOAD_VAL - cmpa_val;
		}
		
		if((HS1_state == 1) && (HS2_state == 0) && (HS3_state == 1))
		{
			TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1 шим
			TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2 земля
			TIM1 -> CCER &= ~(TIM_CCER_CC3E | TIM_CCER_CC3NE); //канал 3 выкл
			TIM1 -> CCR1 = ARELOAD_VAL - cmpa_val;
			TIM1 -> CCR2 = ARELOAD_VAL;
			TIM1 -> CCR3 = 0;
		}
	}
	else
	{
			TIM1 -> CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC1NE); //канал 1 выкл
			TIM1 -> CCER &= ~(TIM_CCER_CC2E | TIM_CCER_CC2NE); //канал 2 выкл
			TIM1 -> CCER &= ~(TIM_CCER_CC3E | TIM_CCER_CC3NE); //канал 3 выкл
			TIM1 -> CCR1 = 0;
			TIM1 -> CCR2 = 0;
			TIM1 -> CCR3 = 0;
	}
	
	old_cmpa_val = cmpa_val;
	
	//сканируем датчики тока по разрешению с команды
	if ((scan_permition) && (cmpa_val > minimum_cmpa)) {
		phase_1[p_i] = phase_current[0];
		//phase_2[p_i] = phase_current[1];
		//phase_3[p_i] = phase_current[2];
		
		p_i++;
		if (p_i > 30000)
		{
			p_i = 0;
			scan_permition = 0;
		}
	}
	
	//GPIOD -> ODR &= ~GPIO_ODR_OD2;
	GPIOB -> ODR &= ~GPIO_ODR_OD6;
}

//Прерывание по переполнению таймера TIM6 (200 Гц)
void TIM6_DAC_IRQHandler (void) {
	TIM6 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
}

//Вспомогательный таймер с пониженным приоритетом (200 Гц)
void TIM5_IRQHandler (void) {
	TIM5 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
	RXBuffer_Handler();
}

//Прерывание USART1
void USART1_IRQHandler(void) {
	//data is transferred to the shift register
	if (USART1 -> ISR & USART_ISR_TXE) {
		if (TXi_t != TXi_w) {
			USART1 -> TDR = TX_BUF[TXi_t];
			TXi_t ++;
			TX_BUF_empty_space++;
	
			if (TXi_t >= 256){ TXi_t = 0; }
		} else {
			USART1 -> CR1 &= ~USART_ISR_TXE; //Запретить прерывания на передачу
		}
	}
	
	//проверяем что был принят бит
	if (USART1 -> ISR & USART_ISR_RXNE){
		RXc = USART1 -> RDR;
		RX_BUF[RXi] = RXc;
		RXi++;

		//Если не символ возврата каретки
		if (RXc != 13) {
			if (RXi > RX_BUF_SIZE-1) {
				clear_RXBuffer();
			}
		} else {
			RX_FLAG_END_LINE = 1;
		}
	}
}

//Прерывание по резервному датчику Холла
void EXTI9_5_IRQHandler(void) {
	EXTI -> PR1 |= EXTI_PR1_PIF7; //сбрасываем прерывание перед самим прерыванием
	TIM16 -> CNT = 0; //отбросим таймер назад
	rotor_speed = TIM15 -> CNT; //измерим скорость вращения другим таймером
	TIM15 -> CNT = 0; //Сбросим в ноль
	rotor_state = ROTATE; //Фиксация факта вращения
	HS_ticks++; //Инкрементируем счётчик срабатываний датчика холла
}

//Прерывание по обновлению состояния ротора
void TIM1_UP_TIM16_IRQHandler(void) {
	TIM16 -> SR &= ~ TIM_SR_UIF; //Сброс флага прерывания
	rotor_state = STOP;
}

void Reset_Handler (void) {
	main();
}

/*____________________FUNCTIONS____________________*/

/**
	* @brief  Инициализация переменных
  * @param  None
  * @retval None
  */
void global_var_init(void) {
	clear_RXBuffer(); //очистим буфер перед работой
	
	p_i = 0;
	scan_permition = 1;
	
	TXi_t = 0;
	TXi_w = 0;
	TX_BUF_empty_space = 256; //При инициализациии буфер пуст
	
	cmpa_val = 0; //скважность 0 по умолчанию
	old_cmpa_val = 0;
	//Инициализация защёлки при включении
	throttle_state_counter = 0;
	throttle_state = 0;
	
	HS_ticks = 0;
	
	brake_1 = 0;
	
	delta_counter = 0;
	delta2_counter = 0;
	
	//Переменные по превышению тока
	current_range = 1400; //Размах трубы допустимого тока
	max_current = 2068 + current_range; //2068 - ноль тока в фазе (в ед АЦП)
	min_current = 2068 - current_range;
}

/**
  * @brief  Фильтр Калмана для фильтрации помех
  * @param  y - предыдущее отфильтрованное измерение
  * 		m - текущее измерение
  * 		k - коэффициент фильтрации
  * @retval Возвращает отфильтрованное измерение
  */
int kalman_filter(uint16_t y, uint16_t m, float k)
{
	return (int) ((m*k)+((1-k)*y));
}

/*___Функциональные блоки___*/




/*___Коммуникации___*/
//очистка буфера приёмника
void clear_RXBuffer(void) {
	for (RXi=0; RXi<RX_BUF_SIZE; RXi++)
		RX_BUF[RXi] = '\0';
	RXi = 0;
	
	input_param[0] = 0;
	input_param[1] = 0;
}
/**
	* @brief  Запись данных в циклический буфер на отправку через USART
	* @param  Массив данных, предназначенных на отправку
  * @retval None
  */
void USART1_TXBuf_append (char *buffer) {
	//Пока в буфере есть данные
	while (*buffer) {
		TX_BUF[TXi_w] = *buffer++;
		TXi_w++;
		TX_BUF_empty_space--;
		
		if (TXi_w >= 256){ TXi_w = 0; }
	}
	
	USART1 -> CR1 |= (1 << 7); //разрешить прерывание на передачу
	
	//блок ниже вроде как не нужен, потому что флаг TXE не может быть сброшен никогда
	/*
	//Если USART ничего не отправляет, то необходимо отправить один бит
	if (USART1 -> ISR & (1 << 7)) //data is transferred to the shift register
	{
		USART1 -> TDR = TX_BUF[TXi_t];
		TXi_t ++;
		TX_BUF_empty_space++;
	}
	*/
	
	if (TXi_t >= 256){ TXi_t = 0; }
}

/**
	* @brief  Перевод числа в аски символы
	* @param  value - число, которое необходимо преобразовать
	*					*buffer - адрес массив, куда писать результат
  * @retval Возвращает буфер с записанным результататом преобразования
  */
char * utoa_div(uint32_t value, char *buffer){
   buffer += 11; 
		// 11 байт достаточно для десятичного представления 32-х битного числа
		// и  завершающего нуля
   *--buffer = 0;
   do
   {
      *--buffer = value % 10 + '0';
      value /= 10;
   }
   while (value != 0);
   return buffer;
}

/**
	* @brief  Функция проверяет по таблице ascii является ли байт цифрой
	* @param  byte - первая строка
	* @retval Возвращает 1, если байт является цифрой
  */
int is_digit(char byte) {
	if ((byte == 0x30) | (byte == 0x31) | (byte == 0x32) | (byte == 0x33) |\
		(byte == 0x34) | (byte == 0x35) | (byte == 0x36) | (byte == 0x37) |\
	(byte == 0x38) | (byte == 0x39)) {
		return 1;
	} else {
		return 0;
	}
};

/**
	* @brief  Функция сравнения двух строк (замена strncmp)
	* @param  string1 - первая строка
	*					string2 - вторая строка
	*					len - длинна сравнения (количество байт)
	* @retval Возвращает 1, если строки равны
  */
int is_equal(char *string1, char *string2, int len) {
	while(len --> 0) {
		if(*string1++ != *string2++) {
			return 0;
		}
	}
	return 1;
};

/**
	* @brief  Поиск в буфере чисел как параметров команды
	* @param  *buf - адрес массива на приём
	* @retval Возвращает 0 при возникновении ошибки
  */
int get_param(char *buf) {
	uint8_t num_of_params = 2;
	uint8_t i = 0;
	uint8_t pos = 0;
	int value = 0;
	
	//Дойдём до первого пробела
	while(buf[pos] != ' ') {
		pos++;
		//Если прошли весь буфер и не нашли пробела
		if (pos >= RX_BUF_SIZE) {
			//Выходим из функции
			return 0;
		}
	}
	
	//Начиная со следующего символа начнём вычислять параметры
	while(buf[++pos] != '\r') {
		if (!(is_digit(buf[pos]) | (buf[pos] == ' '))) {
			USART1_TXBuf_append("Read error!\r");
			clear_RXBuffer();
			return 0;
		}
		
		if (buf[pos] != ' ') {
			value = value*10 + (buf[pos] - 48);
		} else {
			input_param[i] = value;
			i++;
			value = 0;
			if (i > num_of_params -1) {
				USART1_TXBuf_append("Read error!\r");
				clear_RXBuffer();
				return 0;
			}
		}
	}
	
	//Запишем параметр, который будет перед концом строки
	input_param[i] = value;
	
	return 1;
};

//Обработка буфера приёма
void RXBuffer_Handler (void) {
	//Если что-то было считано в буфер команды
	if (RX_FLAG_END_LINE == 1) {
		
		//Вычислим параметры у команды
		get_param(RX_BUF);
		
		// Reset END_LINE Flag
		RX_FLAG_END_LINE = 0;
		
		//Чекнуть скорость вращения ротора
		if (is_equal(RX_BUF, "rate\r", 6)) {
			rate_speed = (int) (300960/rotor_speed);
			USART1_TXBuf_append(utoa_div(rate_speed,buffer_));
			USART1_TXBuf_append("\n");
			USART1_TXBuf_append(utoa_div(rotor_speed,buffer_));
			USART1_TXBuf_append("\n");
			USART1_TXBuf_append(utoa_div(cmpa_val,buffer_));
			USART1_TXBuf_append("\n");
		}
		
		//Проверить внутренние параметры блока
		if (is_equal(RX_BUF, "status\r", 6)) {
			if (rotor_state == STOP) {
				USART1_TXBuf_append("Current range: ");
				USART1_TXBuf_append(utoa_div(current_range,buffer_));
				USART1_TXBuf_append("\n");
				USART1_TXBuf_append("Max current: ");
				USART1_TXBuf_append(utoa_div(max_current,buffer_));
				USART1_TXBuf_append("\n");
				USART1_TXBuf_append("Min current: ");
				USART1_TXBuf_append(utoa_div(min_current,buffer_));
				USART1_TXBuf_append("\n");
				USART1_TXBuf_append("Temperature: ");
				USART1_TXBuf_append(utoa_div(((int)((temp_1/10.17)-273)),buffer_));
				USART1_TXBuf_append("\n");
				//USART1_TXBuf_append(utoa_div(((int)(122*(3.3*(temp_1/4095))-273)),buffer_));
				//USART1_TXBuf_append("\n");
			}
		}
		
		//Проверить внутренние параметры блока
		if (is_equal(RX_BUF, "change_cur", 10)) {
			if ((input_param[0] < 2000) && (input_param[0] > 200)) {
				current_range = input_param[0]; //Размах трубы допустимого тока
				max_current = 2068 + current_range; //2068 - ноль тока в фазе (в ед АЦП)
				min_current = 2068 - current_range;
				USART1_TXBuf_append("Done");
				USART1_TXBuf_append("\n");
			} else { 
				USART1_TXBuf_append("Sasha hvatit menya proveryat");
				USART1_TXBuf_append("\n");
			}
		}
		
		//Проверить значение одометра
		if (is_equal(RX_BUF, "odometer\r", 9)) {
			USART1_TXBuf_append(utoa_div(((int)HS_ticks/16),buffer_));
			USART1_TXBuf_append("\n");
		}
		
		//Проверить значение одометра
		if (is_equal(RX_BUF, "zeros\r", 6)) {
			HS_ticks = 0;
			USART1_TXBuf_append("Done");
			USART1_TXBuf_append("\n");
		}
		
		//отправка массива для теста работы фазного датчика #
		if (is_equal(RX_BUF, "phase_data1\r", 12)) {
			uint16_t i;
			i = 0;
			while(i <= 29999) {
				USART1_TXBuf_append(utoa_div(phase_1[i],buffer_));
				USART1_TXBuf_append("\n");
				SysTick_wait(48000);//Ждём
				i++;
			}
		}
		
//		//отправка массива для теста работы фазного датчика #
//		if (is_equal(RX_BUF, "phase_data2\r", 12)) {
//			uint16_t i;
//			i = 0;
//			while(i <= 599) {
//				USART1_TXBuf_append(utoa_div(phase_2[i],buffer_));
//				USART1_TXBuf_append("\n");
//				SysTick_wait(48000);//Ждём
//				i++;
//			}
//		}
//		
//		//отправка массива для теста работы фазного датчика #
//		if (is_equal(RX_BUF, "phase_data3\r", 12)) {
//			uint16_t i;
//			i = 0;
//			while(i <= 599) {
//				USART1_TXBuf_append(utoa_div(phase_3[i],buffer_));
//				USART1_TXBuf_append("\n");
//				SysTick_wait(48000);//Ждём
//				i++;
//			}
//		}
		
		//Разрешить сканировать переменную #
		if (is_equal(RX_BUF, "scan\r", 5)) {
			if (scan_permition) {
				USART1_TXBuf_append("Scan off\n");
				scan_permition = 0;
			} else {
				USART1_TXBuf_append("Scan on\n");
				scan_permition = 1;
			}
		}
				
		clear_RXBuffer();
	}
}
