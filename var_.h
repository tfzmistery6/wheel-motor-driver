#include "stm32g474xx.h"

//буфер считываемой команды
#define RX_BUF_SIZE 80
char RX_FLAG_END_LINE = 0; //флаг принятия конца строки
char RXi; //указатель на элеммент массива на чтение
char RXc; //буфер для читаемого символа
char RX_BUF[RX_BUF_SIZE] = {'\0'}; //массив на чтение

uint16_t input_param[2]; //Буфер параметров команды принятой в USART1

char buffer_[12]; //Буфер для чисел, которые необходимо вывести в USART1

//Циклический буфер на отправку
#define TX_BUF_SIZE 256
int TX_BUF_empty_space; //Колчиство свободных байт в TX_BUF
int TXi_w; //Указатель куда писать байт в массив
int TXi_t; //Указатель откуда отправлять байт в массив
char TX_BUF[TX_BUF_SIZE] = {'\0'}; //Циклический массив для отправки данных по USART

//Измерения по АЦП
uint16_t phase_current[3]; //Фазные токи (АЦП1)
uint16_t temperature[4]; //Темпертуры мотора и радиаторов (АЦП2) %
uint16_t raw_throttle; //Измерение ручки газа (в еденицах ADC2)
uint16_t throttle; //Обработанный сигнал с ручки газа (цифровая фильтация)
uint16_t cmpa_val; //Преобразованное значение с ручки
uint16_t temp_1; //температур а радиатора (на схеме Temp_1)

_Bool overcur_flag; 
uint16_t max_current;
uint16_t min_current;
uint16_t current_range;

uint16_t sup_voltage; //Напряжение питания в единицах АЦП
uint16_t battery_voltage; //Напряжение питания в вольтах

uint16_t phase_1[30000] = {0}; //фазный ток 1 для тестов #
//uint16_t phase_2[600] = {0}; //фазный ток 2 для тестов #
//uint16_t phase_3[600] = {0}; //фазный ток 3 для тестов #
uint16_t p_i; //#
_Bool scan_permition; //#
uint16_t tim7_counter; //тестирование датчика тока#

uint16_t test_counter; //тестовый счётчик для дебага #

//Переменные для обработки спада ШИМ
uint16_t old_cmpa_val;//Старый ШИМ
uint16_t delta_counter; //счётчик веремени при спаде ШИМ
uint16_t delta2_counter; //счётчик времени при нарастании ШИМ
#define minimum_cmpa 6 //минимальный референсный cmpa_val

uint16_t brake_cmpa;

_Bool brake_1;
_Bool brake_2;

//Главные переменные для работы алгоримта
_Bool error_state;
_Bool throttle_state;
uint16_t throttle_state_counter;
#define OK 1

uint8_t rotor_state;
#define STOP 0
#define ROTATE 1

uint16_t rotor_speed; //Здесь скорость в тиках
uint16_t rate_speed; //Сюда записываем об/мин

//Состояния датчиков холла
_Bool HS1_state;
_Bool HS2_state;
_Bool HS3_state;

_Bool HS1_state_prev;
_Bool HS2_state_prev;
_Bool HS3_state_prev;

uint32_t HS_ticks;

#define ARELOAD_VAL 255

