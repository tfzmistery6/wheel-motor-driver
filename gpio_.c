#include "stm32g474xx.h"

void gpio_init(void) {
	/*___ПОРТ A___*/
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
	
	//PA8 PA9 PA10 - Датчики Холла (основные)
	//PA8 - HS3
	//PA9 - HS2 
	//PA10 - HS1
	
	//PA8 PA9 PA10 input mode
	GPIOA -> MODER &= ~(1 << 16 | 1 << 17 | 1 << 18 | 1 << 19 | 1 << 20 | 1 << 21);
	//PA8 PA9 PA10 very high speed
	GPIOA -> OSPEEDR |= (1 << 16 | 1 << 17 | 1 << 18 | 1 << 19 | 1 << 20 | 1 << 21);
	
	/*___ПОРТ D___*/
	//Включение тактирования порта D
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIODEN;
	
	//PD2 PP mode (тестовый светодиод)
	GPIOD -> MODER &= ~(1 << 5);
	
	/*___ПОРТ B___*/
	//Настроим выходы для управления платой защиты
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOBEN; //Тактирование порта B
	
	//PB5 - ВКЛ/ВЫКЛ выхода ШИМ
	GPIOB -> MODER &= ~(1 << 10 | 1 << 11);
	GPIOB -> MODER |= (1 << 10); //output mode
	GPIOB -> ODR |= GPIO_ODR_OD5; //номинальное состояние еденичка
	
	//PB6 - вход ручки тормоза brake_1
	//Сделаем его выходом для дебага прерываний
	GPIOB -> MODER &= ~(1 << 13); //output
	GPIOB -> OSPEEDR |= (1 << 12 | 1 << 13); //very high speed
	GPIOB -> PUPDR |= (1 <<12); //pull up
	
	//PB7 - вход тормозов brake_2
	GPIOB -> MODER &= ~(1 << 14 | 1 << 15); //input
	GPIOB -> OSPEEDR |= (1 << 14 | 1 << 15); //very high speed
	
	/*___ПОРТ C___*/
	RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOCEN; //Тактирование порта C
	
	//PC6 PC7 PC8 - датчики Холла (резервные)
	//PC8 	- HS3_R
	//PC7 	- HS2_R
	//PC6 	- HS1_R
	
	//PC6 PC7 PC8 input mode
	GPIOC -> MODER &= ~(1 << 12 | 1 << 13 | 1 << 14 | 1 << 15 | 1 << 16 | 1 << 17);
	//PC6 PC7 PC8 very high speed
	GPIOC -> OSPEEDR |= (1 << 12 | 1 << 13 | 1 << 14 | 1 << 15 | 1 << 16 | 1 << 17);
	
}
