#include "stm32g474xx.h"
#include "rcc_.h"

/** 
 * @brief  ��������� ������������ �����������
 * @param  none.
 * @return none.
 * ����������� ����� ������ ����������� �������� 16 ���.
 * � ������ ������� �� ����������� �� ������������ ������� �������
 * 8 ��� ����� ���� x32 � ����������� ���������� PLLR /2, AHB presc /8.
 * � ����� ������� SysClk 128 ��� � ������������ ��������� 16 ���.
 */
void sysclk_cfgr(void) {
	FLASH -> ACR |= FLASH_ACR_LATENCY_4WS; // 4 wait states (�� ������� � ���������)
	
	//��� ���� �������� �����������
	while (!(FLASH -> ACR & FLASH_ACR_LATENCY_4WS)) {
	}
	
	RCC -> CR |= RCC_CR_HSEON;
	while(!(RCC -> CR & RCC_CR_HSERDY)) {
	}
	
	RCC -> PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE;
	RCC -> PLLCFGR &= ~(1 << 12); //������� �������� ���
	RCC -> PLLCFGR |= RCC_PLLCFGR_PLLN_5; //��������� ������� x32
	RCC -> PLLCFGR |= RCC_PLLCFGR_PLLREN; //�������� ����� R (�� ���������� �������� /2)
	RCC -> PLLCFGR |= (1 << 28 | 1 << 29 | 1 << 30 | 1 << 31); //�������� ������ PLLP /30
	RCC -> PLLCFGR |= RCC_PLLCFGR_PLLPEN; //�������� ����� P
	RCC -> CCIPR |= (1 << 28); //PLLP selected as ADC1/2 clock
	
	RCC -> CR |= RCC_CR_PLLON;
	while(!(RCC -> CR & RCC_CR_PLLRDY)) {
	}
	
	RCC -> CFGR |= RCC_CFGR_SW_PLL;
	while(!(RCC -> CFGR & RCC_CFGR_SWS_PLL)) {
	}
	
	RCC -> CFGR |= RCC_CFGR_HPRE_DIV8;
}
