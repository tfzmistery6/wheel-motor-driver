#include "stm32g474xx.h"

//Настройка контроллера внешних прерываний
void exti_init(void) {
	
	RCC -> APB2ENR |= RCC_APB2ENR_SYSCFGEN;//Включаем SYSCFG
	SYSCFG -> EXTICR[2] |= (1 << 1); //EXTI8 - PC8 резервный датчик холла (HS3_R)
	
	//Настройка контроллера внешних прерываний
	//EXTI8
	EXTI -> RTSR1 |= EXTI_RTSR1_RT8; //rising trigger event
	EXTI -> PR1 |= EXTI_PR1_PIF8; //сбрасываем прерывание перед самим прерыванием
	EXTI -> IMR1 |= EXTI_IMR1_IM8; //включаем прерывание
};
